﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeaseContext.cs" company="">
//   
// </copyright>
// <summary>
//   The lease context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp.Data
{
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    ///     The lease context.
    /// </summary>
    public class LeaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeaseContext"/> class.
        /// </summary>
        public LeaseContext()
            : this("Default")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeaseContext"/> class.
        /// </summary>
        /// <param name="nameOrConnectionString">
        /// The name or connection string.
        /// </param>
        public LeaseContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        /// <summary>
        /// Gets or sets the dhcp leases.
        /// </summary>
        public DbSet<DhcpLease> DhcpLeases { get; set; }
    }
}