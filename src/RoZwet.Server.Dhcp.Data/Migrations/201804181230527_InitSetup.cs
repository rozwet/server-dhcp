namespace RoZwet.Server.Dhcp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitSetup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DhcpLeases",
                c => new
                    {
                        MacAddress = c.String(nullable: false, maxLength: 17),
                        DateAdded = c.DateTimeOffset(nullable: false, precision: 7),
                        DateExpires = c.DateTimeOffset(nullable: false, precision: 7),
                        IpAddress = c.String(maxLength: 15),
                        Vendor = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.MacAddress);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DhcpLeases");
        }
    }
}
