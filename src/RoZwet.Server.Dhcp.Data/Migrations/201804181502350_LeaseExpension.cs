namespace RoZwet.Server.Dhcp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaseExpension : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DhcpLeases", "Description", c => c.String(maxLength: 256));
            AddColumn("dbo.DhcpLeases", "HostName", c => c.String(maxLength: 256));
            AddColumn("dbo.DhcpLeases", "MacVendor", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DhcpLeases", "MacVendor");
            DropColumn("dbo.DhcpLeases", "HostName");
            DropColumn("dbo.DhcpLeases", "Description");
        }
    }
}
