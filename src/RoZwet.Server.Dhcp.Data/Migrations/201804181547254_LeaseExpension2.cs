namespace RoZwet.Server.Dhcp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaseExpension2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DhcpLeases", "DateExpires", c => c.DateTimeOffset(precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DhcpLeases", "DateExpires", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
    }
}
