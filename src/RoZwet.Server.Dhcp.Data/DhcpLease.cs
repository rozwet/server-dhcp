﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DhcpLease.cs" company="">
//   
// </copyright>
// <summary>
//   The dhcp lease.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The dhcp lease.
    /// </summary>
    public class DhcpLease
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [StringLength(256)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the host name.
        /// </summary>
        [StringLength(256)]
        public string HostName { get; set; }

        /// <summary>
        /// Gets or sets the mac vendor.
        /// </summary>
        [StringLength(256)]
        public string MacVendor { get; set; }

        /// <summary>
        /// Gets or sets the date added.
        /// </summary>
        public DateTimeOffset DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the date expires.
        /// </summary>
        public DateTimeOffset? DateExpires { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        [StringLength(15, MinimumLength = 7)]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the mac address.
        /// </summary>
        [Key]
        [StringLength(17, MinimumLength = 17)]
        public string MacAddress { get; set; }

        /// <summary>
        /// Gets or sets the vendor.
        /// </summary>
        [StringLength(256)]
        public string Vendor { get; set; }
    }
}