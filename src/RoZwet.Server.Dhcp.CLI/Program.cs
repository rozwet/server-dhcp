﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp.CLI
{
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;

    using NetTools;

    using RoZwet.Server.Dhcp.Data;

    using Configuration = RoZwet.Server.Dhcp.Data.Migrations.Configuration;

    /// <summary>
    ///     The program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        ///     The lease expires.
        /// </summary>
        private static readonly TimeSpan leaseExpires = TimeSpan.FromMinutes(180);

        /// <summary>
        ///     The ip address.
        /// </summary>
        private static IPAddress ipAddress;

        /// <summary>
        /// The network interface.
        /// </summary>
        private static NetworkInterface networkInterface;

        private static IPAddress routerIp;

        /// <summary>
        /// The create or renew lease.
        /// </summary>
        /// <param name="mac">
        ///     The mac.
        /// </param>
        /// <param name="vendor">
        ///     The vendor.
        /// </param>
        /// <param name="hostname">
        /// </param>
        /// <param name="vendorId">
        /// </param>
        /// <param name="requestedIp">
        /// </param>
        /// <param name="getMsgType"></param>
        /// <returns>
        /// The <see cref="DhcpLease"/>.
        /// </returns>
        private static DhcpLease CreateOrRenewLease(string mac, string vendor, string hostname, string vendorId, IPAddress requestedIp, DhcpMsgType getMsgType)
        {
            DhcpLease lease;
            using (var leaseContext = new LeaseContext())
            {
                var now = DateTimeOffset.UtcNow;
                leaseContext.DhcpLeases.RemoveRange(
                    leaseContext.DhcpLeases.Where(
                        dhcpLease => dhcpLease.DateExpires <= now && dhcpLease.DateExpires != null));
                leaseContext.SaveChanges();

                lease = leaseContext.DhcpLeases.FirstOrDefault(l => l.MacAddress == mac);

                if (lease == null)
                {
                    lease = new DhcpLease
                                {
                                    MacAddress = mac,
                                    IpAddress = GetNextIpAddress(leaseContext, requestedIp).ToString(),
                                    Vendor = vendorId,
                                    DateAdded = DateTimeOffset.UtcNow,
                                    DateExpires = DateTimeOffset.UtcNow.Add(leaseExpires),
                                    HostName = hostname,
                                };

                    try
                    {
                        lease.MacVendor = new WebClient().DownloadString("http://api.macvendors.com/" + mac);
                    }
                    catch
                    {
                        // ignored;
                    }

                    leaseContext.DhcpLeases.Add(lease);
                }
                else
                {
                    if (lease.DateExpires != null)
                    {
                        lease.DateExpires = DateTimeOffset.UtcNow.Add(leaseExpires);
                    }

                    lease.Vendor = vendorId;
                    lease.HostName = hostname;
                    if (string.IsNullOrEmpty(lease.MacVendor))
                    {
                        try
                        {
                            lease.MacVendor = new WebClient().DownloadString("http://api.macvendors.com/" + mac);
                        }
                        catch
                        {
                            // ignored;
                        }
                    }
                }

                if (getMsgType == DhcpMsgType.DhcpRequest)
                {
                    leaseContext.SaveChanges();
                }
            }

            if (lease.DateExpires == null)
            {
                lease.DateExpires = DateTimeOffset.UtcNow.Add(leaseExpires);
            }

            return lease;
        }

        /// <summary>
        /// The get next ip address.
        /// </summary>
        /// <param name="leaseContext">
        /// The context.
        /// </param>
        /// <param name="requestedIp">
        /// </param>
        /// <returns>
        /// The <see cref="IPAddress"/>.
        /// </returns>
        private static IPAddress GetNextIpAddress(LeaseContext leaseContext, IPAddress requestedIp)
        {
            const int Start = 100;

            //if (requestedIp != null)
            //{
            //    var range = new IPAddressRange(IPAddress.Parse($"192.168.2.{Start}"), IPAddress.Parse("192.168.2.199"));
            //    if (range.Contains(requestedIp))
            //    {
            //        if (!leaseContext.DhcpLeases.Select(lease => lease.IpAddress).Contains(requestedIp.ToString()))
            //        {
            //            return requestedIp;
            //        }
            //    }
            //}

            for (var i = Start; i < 200; i++)
            {
                var newIp = $"192.168.2.{i}";
                if (!leaseContext.DhcpLeases.Select(lease => lease.IpAddress).Contains(newIp))
                {
                    return IPAddress.Parse(newIp);
                }
            }

            return IPAddress.None;
        }

        /// <summary>
        ///     The init lease context.
        /// </summary>
        private static void InitLeaseContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LeaseContext, Configuration>());
            using (var context = new LeaseContext())
            {
                context.Database.Initialize(true);
            }
        }

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        private static void Main(string[] args)
        {
            Console.Write(@"Initializing database... ");
            InitLeaseContext();
            Console.WriteLine(@"Done!");

            Console.Write(@"Initializing network interface... ");
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces().OrderBy(x => x.Id).ToArray();
            
            Console.WriteLine("Select interface: ");
            for (int i = 0; i < networkInterfaces.Length; i++)
            {
                
                var isOn = networkInterfaces[i].OperationalStatus == OperationalStatus.Up;
                Console.ForegroundColor = isOn ? ConsoleColor.Green : ConsoleColor.Red;
                Console.Write($"{i}: [{(isOn ? "ON" : "OFF")}] ");
                Console.ResetColor();
                Console.WriteLine(networkInterfaces[i].Name + " " + networkInterfaces[i].Id);
            }
            if (ConfigurationManager.AppSettings["NetworkInterface"] == "-1")
            {
                networkInterface = networkInterfaces[Convert.ToInt32(Console.ReadLine())];
            }
            else
            {
                networkInterface =
                    networkInterfaces.First(x => x.Id == ConfigurationManager.AppSettings["NetworkInterface"]);
            }

            Console.WriteLine($@"Found interface {networkInterface?.Id} {networkInterface?.Name} ({networkInterface?.Description})!");

            Console.Write(@"Initializing ip address... ");
            ipAddress = IPAddress.Parse(ConfigurationManager.AppSettings["ListenIPAddress"]);
            Console.WriteLine($@"Found ip address {ipAddress}!");

            routerIp = IPAddress.Parse(ConfigurationManager.AppSettings["GatewayIPAddress"]);

            var server = new DhcpServer(ipAddress) { ServerName = Environment.MachineName };
            server.OnDataReceived += Request;
            server.BroadcastAddress = IPAddress.Broadcast;
            server.SendDhcpAnswerNetworkInterface = networkInterface;
            server.UnhandledException += exception =>
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(exception.Message);
                    Console.WriteLine();
                    Console.ResetColor();
                };
            server.Start();
            Console.WriteLine($@"Running DHCP server {server.ServerName} on {ipAddress}. Press enter to stop it.");
            Console.ReadLine();
            server.Dispose();
        }

        /// <summary>
        /// The request.
        /// </summary>
        /// <param name="dhcpRequest">
        /// The dhcp request.
        /// </param>
        private static void Request(DhcpRequest dhcpRequest)
        {
            var mac = MacAddressFormatter.Format(dhcpRequest.GetChaddr());

            var hostName = string.Empty;
            try
            {
                hostName = dhcpRequest.GetAllOptions().First(pair => pair.Key == DhcpOption.HostName).Value.AsString().Split('(')
                    [1].Trim().TrimEnd(')');
            }
            catch (Exception)
            {
                //ignored
            }


            var vendorId = string.Empty;
            try
            {
                vendorId = dhcpRequest.GetAllOptions().First(pair => pair.Key == DhcpOption.Vendorclassidentifier).Value.AsString()
                    .Split('(')[1].Trim().TrimEnd(')');
            }
            catch (Exception)
            {
                //ignored
            }

            var requestedIp = dhcpRequest.GetRequestedIP();
            var lease = CreateOrRenewLease(mac, null, hostName, vendorId, requestedIp, dhcpRequest.GetMsgType());

            Console.WriteLine(
                $@"{new string(dhcpRequest.GetMsgType().ToString().Skip(4).ToArray())} from {lease.MacAddress} ({lease.MacVendor}), it will be {
                        lease.IpAddress
                    }");

            var leaseInSeconds = lease.DateExpires?.Subtract(DateTimeOffset.UtcNow).TotalSeconds;
            var replyOptions = new DhcpReplyOptions
                                   {
                                       SubnetMask = IPAddress.Parse("255.255.255.0"),
                                       DomainName = "rozwet.local",
                                       ServerIdentifier = ipAddress,
                                       ServerIpAddress = ipAddress,
                                       RouterIP =
                                           routerIp,
                                       DomainNameServers =
                                           networkInterface.GetIPProperties().DnsAddresses.ToArray(),
                                       IPAddressLeaseTime = Convert.ToUInt32(leaseInSeconds),
                                       RenewalTimeValueT1 = Convert.ToUInt32(leaseInSeconds * 0.5),
                                       RebindingTimeValueT2 = Convert.ToUInt32(leaseInSeconds * 0.875)
                                   };

            // Options should be filled with valid data. Only requested options will be sent.
            // Some static routes

            // Lets send reply to client!
            if (dhcpRequest.GetMsgType() == DhcpMsgType.DhcpDiscover)
            {
                dhcpRequest.SendDhcpReply(DhcpMsgType.DhcpOffer, IPAddress.Parse(lease.IpAddress), replyOptions);
            }

            if (dhcpRequest.GetMsgType() == DhcpMsgType.DhcpRequest)
            {
                dhcpRequest.SendDhcpReply(DhcpMsgType.DhcPack, IPAddress.Parse(lease.IpAddress), replyOptions);
            }
        }
    }
}