// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DhcpServer.cs" company="">
//   
// </copyright>
// <summary>
//   DHCP Server
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    using System;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Threading;

    /// <summary>
    ///     DHCP Server
    /// </summary>
    public class DhcpServer : IDisposable
    {
        /// <summary>
        /// The por t_ t o_ liste n_ to.
        /// </summary>
        private const int PortToListenTo = 67;

        /// <summary>
        /// The _bind ip.
        /// </summary>
        private readonly IPAddress bindIp;

        /// <summary>
        /// The receive data thread.
        /// </summary>
        private Thread receiveDataThread;

        /// <summary>
        /// The socket.
        /// </summary>
        private Socket socket;

        /// <summary>
        /// Initializes a new instance of the <see cref="DhcpServer"/> class. 
        /// Creates DHCP server, it will be started instantly
        /// </summary>
        /// <param name="bindIp">
        /// IP address to bind
        /// </param>
        public DhcpServer(IPAddress bindIp)
        {
            this.bindIp = bindIp;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DhcpServer"/> class. Creates DHCP server, it will be started instantly
        /// </summary>
        public DhcpServer()
            : this(IPAddress.Any)
        {
            this.BroadcastAddress = IPAddress.Broadcast;
        }

        /// <summary>Delegate for DHCP message</summary>
        public delegate void DhcpDataReceivedEventHandler(DhcpRequest dhcpRequest);

        /// <summary>Will be called on any DHCP message</summary>
        public event DhcpDataReceivedEventHandler OnDataReceived = delegate { };

        /// <summary>Will be called on any DECLINE message</summary>
        public event DhcpDataReceivedEventHandler OnDecline = delegate { };

        /// <summary>Will be called on any DISCOVER message</summary>
        public event DhcpDataReceivedEventHandler OnDiscover = delegate { };

        /// <summary>Will be called on any DECLINE inform</summary>
        public event DhcpDataReceivedEventHandler OnInform = delegate { };

        /// <summary>Will be called on any DECLINE released</summary>
        public event DhcpDataReceivedEventHandler OnReleased = delegate { };

        /// <summary>Will be called on any REQUEST message</summary>
        public event DhcpDataReceivedEventHandler OnRequest = delegate { };

        /// <summary>
        /// The unhandled exception.
        /// </summary>
        public event Action<Exception> UnhandledException;

        /// <summary>
        /// Gets or sets the broadcast address.
        /// </summary>
        public IPAddress BroadcastAddress { get; set; }

        /// <summary>
        /// Gets or sets the send dhcp answer network interface.
        /// </summary>
        public NetworkInterface SendDhcpAnswerNetworkInterface { get; set; }

        /// <summary>Server name (optional)</summary>
        public string ServerName { get; set; }

        /// <summary>Disposes DHCP server</summary>
        public void Dispose()
        {
            if (this.socket != null)
            {
                this.socket.Close();
                this.socket = null;
            }

            if (this.receiveDataThread != null)
            {
                this.receiveDataThread.Abort();
                this.receiveDataThread = null;
            }
        }

        /// <summary>
        /// The start.
        /// </summary>
        public void Start()
        {
            var ipLocalEndPoint = new IPEndPoint(this.bindIp, PortToListenTo);
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            this.socket.Bind(ipLocalEndPoint);
            this.receiveDataThread = new Thread(this.ReceiveDataThread);
            this.receiveDataThread.Start();
        }

        /// <summary>
        /// The data received.
        /// </summary>
        /// <param name="o">
        /// The o.
        /// </param>
        private void DataReceived(object o)
        {
            var data = (byte[])o;
            try
            {
                var dhcpRequest = new DhcpRequest(data, this.socket, this);

                // ccDHCP = new clsDHCP();

                // data is now in the structure
                // get the msg type
                this.OnDataReceived(dhcpRequest);
                var msgType = dhcpRequest.GetMsgType();

                switch (msgType)
                {
                    case DhcpMsgType.DhcpDiscover:
                        this.OnDiscover(dhcpRequest);
                        break;
                    case DhcpMsgType.DhcpRequest:
                        this.OnRequest(dhcpRequest);
                        break;
                    case DhcpMsgType.DhcpDecline:
                        this.OnDecline(dhcpRequest);
                        break;
                    case DhcpMsgType.DhcpRelease:
                        this.OnReleased(dhcpRequest);
                        break;
                    case DhcpMsgType.DhcpInform:
                        this.OnInform(dhcpRequest);
                        break;

                    // default:
                    // Console.WriteLine("Unknown DHCP message: " + (int)MsgTyp + " (" + MsgTyp.ToString() + ")");
                    // break;
                }
            }
            catch (Exception ex)
            {
                this.UnhandledException?.Invoke(ex);
            }
        }

        /// <summary>
        /// The receive data thread.
        /// </summary>
        private void ReceiveDataThread()
        {
            while (true)
            {
                try
                {
                    var sender = new IPEndPoint(IPAddress.Any, 0);
                    EndPoint remote = sender;
                    var buffer = new byte[1024];
                    var len = this.socket.ReceiveFrom(buffer, ref remote);
                    if (len <= 0)
                    {
                        continue;
                    }

                    Array.Resize(ref buffer, len);
                    var dataReceivedThread = new Thread(this.DataReceived);
                    dataReceivedThread.Start(buffer);
                }
                catch (Exception ex)
                {
                    this.UnhandledException?.Invoke(ex);
                }
            }
        }
    }
}