// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DHCPMsgType.cs" company="">
//   
// </copyright>
// <summary>
//   DHCP message type
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    /// <summary>DHCP message type</summary>
    public enum DhcpMsgType
    {
        /// <summary>DHCP DISCOVER message</summary>
        DhcpDiscover = 1,

        /// <summary>DHCP OFFER message</summary>
        DhcpOffer = 2,

        /// <summary>DHCP REQUEST message</summary>
        DhcpRequest = 3,

        /// <summary>DHCP DECLINE message</summary>
        DhcpDecline = 4,

        /// <summary>DHCP ACK message</summary>
        DhcPack = 5,

        /// <summary>DHCP NAK message</summary>
        DhcpNak = 6,

        /// <summary>DHCP RELEASE message</summary>
        DhcpRelease = 7,

        /// <summary>DHCP INFORM message</summary>
        DhcpInform = 8
    }
}