// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RelayInfo.cs" company="">
//   
// </copyright>
// <summary>
//   DHCP relay information (option 82)
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    /// <summary>DHCP relay information (option 82)</summary>
    public struct RelayInfo
    {
        /// <summary>Agent circuit ID</summary>
        public byte[] AgentCircuitId;

        /// <summary>Agent remote ID</summary>
        public byte[] AgentRemoteId;
    }
}