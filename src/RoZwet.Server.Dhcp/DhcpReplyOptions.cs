// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DHCPReplyOptions.cs" company="">
//   
// </copyright>
// <summary>
//   Reply options
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    using System.Collections.Generic;
    using System.Net;

    /// <summary>Reply options</summary>
    public class DhcpReplyOptions
    {
        /// <summary>Domain name</summary>
        public string DomainName = null;

        /// <summary>Domain name servers (DNS)</summary>
        public IPAddress[] DomainNameServers = null;

        /// <summary>IP address lease time (seconds)</summary>
        public uint IPAddressLeaseTime = 60 * 60 * 24;

        /// <summary>Log server IP</summary>
        public IPAddress LogServerIP = null;

        /// <summary>Other options which will be sent on request</summary>
        public Dictionary<DhcpOption, byte[]> OtherRequestedOptions = new Dictionary<DhcpOption, byte[]>();

        /// <summary>Rebinding time (seconds)</summary>
        public uint? RebindingTimeValueT2 = 60 * 60 * 24;

        /// <summary>Renewal time (seconds)</summary>
        public uint? RenewalTimeValueT1 = 60 * 60 * 24;

        /// <summary>Router (gateway) IP</summary>
        public IPAddress RouterIP = null;

        /// <summary>IP address of DHCP server</summary>
        public IPAddress ServerIdentifier = null;

        /// <summary>Next Server IP address (bootp)</summary>
        public IPAddress ServerIpAddress = null;

        /// <summary>Static routes</summary>
        public NetworkRoute[] StaticRoutes = null;

        /// <summary>IP address</summary>
        public IPAddress SubnetMask = null;
    }
}