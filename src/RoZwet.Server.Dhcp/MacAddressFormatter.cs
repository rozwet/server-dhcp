﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoZwet.Server.Dhcp
{
    public class MacAddressFormatter
    {
        private MacAddressFormatter()
        {
            
        }

        public static string Format(byte[] bytes)
        {
            var address = bytes.AsString();
            if (address == null)
            {
                return null;
            }

            var array = address.Split(' ')[0].ToCharArray();
            var sb = new StringBuilder();
            for (var i = 0; i < array.Length; i++)
            {
                if (i > 0 && i % 2 == 0)
                {
                    sb.Append(":");
                }
                sb.Append(array[i]);
            }
            return sb.ToString();
        }
    }
}
