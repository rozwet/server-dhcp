// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DHCPRequest.cs" company="">
//   
// </copyright>
// <summary>
//   DHCP request
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    /// <summary>
    ///     DHCP request
    /// </summary>
    public class DhcpRequest
    {
        /// <summary>
        /// The optio n_ offset.
        /// </summary>
        private const int OptionOffset = 240;

        /// <summary>
        /// The por t_ t o_ sen d_ t o_ client.
        /// </summary>
        private const int PortToSendToClient = 68;

        /// <summary>
        /// The por t_ t o_ sen d_ t o_ relay.
        /// </summary>
        private const int PortToSendToRelay = 67;

        /// <summary>
        /// The dhcp server.
        /// </summary>
        private readonly DhcpServer dhcpServer;

        /// <summary>
        /// The request data.
        /// </summary>
        private readonly DhcpPacket requestData;

        /// <summary>
        /// The request socket.
        /// </summary>
        private readonly Socket requestSocket;

        /// <summary>
        /// Initializes a new instance of the <see cref="DhcpRequest"/> class.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="socket">
        /// The socket.
        /// </param>
        /// <param name="server">
        /// The server.
        /// </param>
        internal DhcpRequest(byte[] data, Socket socket, DhcpServer server)
        {
            this.dhcpServer = server;
            BinaryReader rdr;
            var stm = new MemoryStream(data, 0, data.Length);
            rdr = new BinaryReader(stm);

            // Reading data
            this.requestData.Op = rdr.ReadByte();
            this.requestData.Htype = rdr.ReadByte();
            this.requestData.Hlen = rdr.ReadByte();
            this.requestData.Hops = rdr.ReadByte();
            this.requestData.Xid = rdr.ReadBytes(4);
            this.requestData.Secs = rdr.ReadBytes(2);
            this.requestData.Flags = rdr.ReadBytes(2);
            this.requestData.Ciaddr = rdr.ReadBytes(4);
            this.requestData.Yiaddr = rdr.ReadBytes(4);
            this.requestData.Siaddr = rdr.ReadBytes(4);
            this.requestData.Giaddr = rdr.ReadBytes(4);
            this.requestData.Chaddr = rdr.ReadBytes(16);
            this.requestData.Sname = rdr.ReadBytes(64);
            this.requestData.File = rdr.ReadBytes(128);
            this.requestData.Mcookie = rdr.ReadBytes(4);
            this.requestData.Options = rdr.ReadBytes(data.Length - OptionOffset);
            this.requestSocket = socket;
        }

        /// <summary>
        ///     Returns all options
        /// </summary>
        /// <returns>Options dictionary</returns>
        public Dictionary<DhcpOption, byte[]> GetAllOptions()
        {
            var result = new Dictionary<DhcpOption, byte[]>();

            for (var i = 0; i < this.requestData.Options.Length; i++)
            {
                var dDataId = (DhcpOption)this.requestData.Options[i];
                if (dDataId == DhcpOption.EndOption)
                {
                    break;
                }

                var dataLength = this.requestData.Options[i + 1];
                var dumpData = new byte[dataLength];
                Array.Copy(this.requestData.Options, i + 2, dumpData, 0, dataLength);
                result[dDataId] = dumpData;

                dataLength = this.requestData.Options[i + 1];
                i += 1 + dataLength;
            }

            return result;
        }

        /// <summary>
        ///     Returns chaddr (client hardware address)
        /// </summary>
        /// <returns>chaddr</returns>
        public byte[] GetChaddr()
        {
            var res = new byte[this.requestData.Hlen];
            Array.Copy(this.requestData.Chaddr, res, this.requestData.Hlen);
            return res;
        }

        /// <summary>
        ///     Returns ciaddr (client IP address)
        /// </summary>
        /// <returns>ciaddr</returns>
        public IPAddress GetCiaddr()
        {
            if (this.requestData.Ciaddr[0] == 0 && this.requestData.Ciaddr[1] == 0 && this.requestData.Ciaddr[2] == 0
                && this.requestData.Ciaddr[3] == 0)
            {
                return null;
            }

            return new IPAddress(this.requestData.Ciaddr);
        }

        /// <summary>
        ///     Returns giaddr (gateway IP address switched by relay)
        /// </summary>
        /// <returns>giaddr</returns>
        public IPAddress GetGiaddr()
        {
            if (this.requestData.Giaddr[0] == 0 && this.requestData.Giaddr[1] == 0 && this.requestData.Giaddr[2] == 0
                && this.requestData.Giaddr[3] == 0)
            {
                return null;
            }

            return new IPAddress(this.requestData.Giaddr);
        }

        /// <summary>
        ///     Returns type of DHCP request
        /// </summary>
        /// <returns>DHCP message type</returns>
        public DhcpMsgType GetMsgType()
        {
            byte[] dData;
            dData = this.GetOptionData(DhcpOption.DhcpMessageType);
            if (dData != null)
            {
                return (DhcpMsgType)dData[0];
            }

            return 0;
        }

        /// <summary>
        /// Returns option content
        /// </summary>
        /// <param name="option">
        /// Option to retrieve
        /// </param>
        /// <returns>
        /// Option content
        /// </returns>
        public byte[] GetOptionData(DhcpOption option)
        {
            var dhcpId = (int)option;
            for (var i = 0; i < this.requestData.Options.Length; i++)
            {
                var dDataId = this.requestData.Options[i];
                if (dDataId == (byte)DhcpOption.EndOption)
                {
                    break;
                }

                byte dataLength;
                if (dDataId == dhcpId)
                {
                    dataLength = this.requestData.Options[i + 1];
                    var dumpData = new byte[dataLength];
                    Array.Copy(this.requestData.Options, i + 2, dumpData, 0, dataLength);
                    return dumpData;
                }

                if (dDataId == 0)
                {
                }
                else
                {
                    dataLength = this.requestData.Options[i + 1];
                    i += 1 + dataLength;
                }
            }

            return null;
        }

        /// <summary>
        ///     Returns entire content of DHCP packet
        /// </summary>
        /// <returns>DHCP packet</returns>
        public DhcpPacket GetRawPacket()
        {
            return this.requestData;
        }

        /// <summary>
        ///     Returns relay info (option 82)
        /// </summary>
        /// <returns>Relay info</returns>
        public RelayInfo? GetRelayInfo()
        {
            var result = new RelayInfo();
            var relayInfo = this.GetOptionData(DhcpOption.RelayInfo);
            if (relayInfo != null)
            {
                var i = 0;
                while (i < relayInfo.Length)
                {
                    var subOptId = relayInfo[i];
                    if (subOptId == 1)
                    {
                        result.AgentCircuitId = new byte[relayInfo[i + 1]];
                        Array.Copy(relayInfo, i + 2, result.AgentCircuitId, 0, relayInfo[i + 1]);
                    }
                    else if (subOptId == 2)
                    {
                        result.AgentRemoteId = new byte[relayInfo[i + 1]];
                        Array.Copy(relayInfo, i + 2, result.AgentRemoteId, 0, relayInfo[i + 1]);
                    }

                    i += 2 + relayInfo[i + 1];
                }

                return result;
            }

            return null;
        }

        /// <summary>
        ///     Returns requested IP (option 50)
        /// </summary>
        /// <returns>Requested IP</returns>
        public IPAddress GetRequestedIP()
        {
            var ipBytes = this.GetOptionData(DhcpOption.RequestedIPAddress);
            return ipBytes == null ? null : new IPAddress(ipBytes);
        }

        /// <summary>
        ///     Returns array of requested by client options
        /// </summary>
        /// <returns>Array of requested by client options</returns>
        public DhcpOption[] GetRequestedOptionsList()
        {
            var reqList = this.GetOptionData(DhcpOption.ParameterRequestList);
            var optList = new List<DhcpOption>();
            if (reqList != null)
            {
                foreach (var option in reqList)
                {
                    optList.Add((DhcpOption)option);
                }
            }
            else
            {
                return null;
            }

            return optList.ToArray();
        }

        /// <summary>
        /// Sends DHCP reply
        /// </summary>
        /// <param name="msgType">
        /// Type of DHCP message to send
        /// </param>
        /// <param name="ip">
        /// IP for client
        /// </param>
        /// <param name="replyData">
        /// Reply options (will be sent if requested)
        /// </param>
        public void SendDhcpReply(DhcpMsgType msgType, IPAddress ip, DhcpReplyOptions replyData)
        {
            this.SendDhcpReply(msgType, ip, replyData, null, null);
        }

        /// <summary>
        /// Sends DHCP reply
        /// </summary>
        /// <param name="msgType">
        /// Type of DHCP message to send
        /// </param>
        /// <param name="ip">
        /// IP for client
        /// </param>
        /// <param name="replyData">
        /// Reply options (will be sent if requested)
        /// </param>
        /// <param name="otherForceOptions">
        /// Force reply options (will be sent anyway)
        /// </param>
        public void SendDhcpReply(
            DhcpMsgType msgType,
            IPAddress ip,
            DhcpReplyOptions replyData,
            Dictionary<DhcpOption, byte[]> otherForceOptions)
        {
            this.SendDhcpReply(msgType, ip, replyData, otherForceOptions, null);
        }

        /// <summary>
        /// Sends DHCP reply
        /// </summary>
        /// <param name="msgType">
        /// Type of DHCP message to send
        /// </param>
        /// <param name="ip">
        /// IP for client
        /// </param>
        /// <param name="replyData">
        /// Reply options (will be sent if requested)
        /// </param>
        /// <param name="forceOptions">
        /// Force reply options (will be sent anyway)
        /// </param>
        public void SendDhcpReply(
            DhcpMsgType msgType,
            IPAddress ip,
            DhcpReplyOptions replyData,
            IEnumerable<DhcpOption> forceOptions)
        {
            this.SendDhcpReply(msgType, ip, replyData, null, forceOptions);
        }

        /// <summary>
        /// The add option element.
        /// </summary>
        /// <param name="fromValue">
        /// The from value.
        /// </param>
        /// <param name="targetArray">
        /// The target array.
        /// </param>
        private static void AddOptionElement(byte[] fromValue, ref byte[] targetArray)
        {
            if (targetArray != null)
            {
                Array.Resize(ref targetArray, targetArray.Length + fromValue.Length);
            }
            else
            {
                Array.Resize(ref targetArray, fromValue.Length);
            }

            Array.Copy(fromValue, 0, targetArray, targetArray.Length - fromValue.Length, fromValue.Length);
        }

        /// <summary>
        /// The build data structure.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private static byte[] BuildDataStructure(DhcpPacket packet)
        {
            var mArray = new byte[0];
            AddOptionElement(new[] { packet.Op }, ref mArray);
            AddOptionElement(new[] { packet.Htype }, ref mArray);
            AddOptionElement(new[] { packet.Hlen }, ref mArray);
            AddOptionElement(new[] { packet.Hops }, ref mArray);
            AddOptionElement(packet.Xid, ref mArray);
            AddOptionElement(packet.Secs, ref mArray);
            AddOptionElement(packet.Flags, ref mArray);
            AddOptionElement(packet.Ciaddr, ref mArray);
            AddOptionElement(packet.Yiaddr, ref mArray);
            AddOptionElement(packet.Siaddr, ref mArray);
            AddOptionElement(packet.Giaddr, ref mArray);
            AddOptionElement(packet.Chaddr, ref mArray);
            AddOptionElement(packet.Sname, ref mArray);
            AddOptionElement(packet.File, ref mArray);

            AddOptionElement(packet.Mcookie, ref mArray);
            AddOptionElement(packet.Options, ref mArray);
            return mArray;
        }

        /// <summary>
        /// The create option element.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        /// <param name="option">
        /// The option.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        private static void CreateOptionElement(ref byte[] options, DhcpOption option, byte[] data)
        {
            byte[] optionData;

            optionData = new byte[data.Length + 2];
            optionData[0] = (byte)option;
            optionData[1] = (byte)data.Length;
            Array.Copy(data, 0, optionData, 2, data.Length);
            if (options == null)
            {
                Array.Resize(ref options, optionData.Length);
            }
            else
            {
                Array.Resize(ref options, options.Length + optionData.Length);
            }

            Array.Copy(optionData, 0, options, options.Length - optionData.Length, optionData.Length);
        }

        /// <summary>
        /// The encode time option.
        /// </summary>
        /// <param name="seconds">
        /// The seconds.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private static byte[] EncodeTimeOption(uint seconds)
        {
            var leaseTime = new byte[4];
            leaseTime[3] = (byte)seconds;
            leaseTime[2] = (byte)(seconds >> 8);
            leaseTime[1] = (byte)(seconds >> 16);
            leaseTime[0] = (byte)(seconds >> 24);
            return leaseTime;
        }

        /// <summary>
        /// The create option struct.
        /// </summary>
        /// <param name="msgType">
        /// The msg type.
        /// </param>
        /// <param name="replyOptions">
        /// The reply options.
        /// </param>
        /// <param name="otherForceOptions">
        /// The other force options.
        /// </param>
        /// <param name="forceOptions">
        /// The force options.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private byte[] CreateOptionStruct(
            DhcpMsgType msgType,
            DhcpReplyOptions replyOptions,
            Dictionary<DhcpOption, byte[]> otherForceOptions,
            IEnumerable<DhcpOption> forceOptions)
        {
            var options = new Dictionary<DhcpOption, byte[]>();

            byte[] resultOptions = null;

            // Requested options
            var reqList = this.GetRequestedOptionsList();

            // Option82?
            var relayInfo = this.GetOptionData(DhcpOption.RelayInfo);
            CreateOptionElement(ref resultOptions, DhcpOption.DhcpMessageType, new[] { (byte)msgType });

            // Server identifier - our IP address
            if (replyOptions != null && replyOptions.ServerIdentifier != null)
            {
                options[DhcpOption.ServerIdentifier] = replyOptions.ServerIdentifier.GetAddressBytes();
            }

            if (reqList == null && forceOptions != null)
            {
                reqList = new DhcpOption[0];
            }

            if (forceOptions == null)
            {
                forceOptions = new DhcpOption[0];
            }

            // Requested options
            if (reqList != null && replyOptions != null)
            {
                foreach (var i in reqList.Union(forceOptions).Distinct().OrderBy(x => (int)x))
                {
                    byte[] optionData = null;

                    // If it's force option - ignore it. We'll send it later.
                    if (otherForceOptions != null && otherForceOptions.TryGetValue(i, out optionData))
                    {
                        continue;
                    }

                    switch (i)
                    {
                        case DhcpOption.SubnetMask:
                            if (replyOptions.SubnetMask != null)
                            {
                                optionData = replyOptions.SubnetMask.GetAddressBytes();
                            }

                            break;
                        case DhcpOption.Router:
                            if (replyOptions.RouterIP != null)
                            {
                                optionData = replyOptions.RouterIP.GetAddressBytes();
                            }

                            break;
                        case DhcpOption.DomainNameServers:
                            if (replyOptions.DomainNameServers != null)
                            {
                                optionData = new byte[] { };
                                foreach (var dns in replyOptions.DomainNameServers)
                                {
                                    var dnsserv = dns.GetAddressBytes();
                                    Array.Resize(ref optionData, optionData.Length + 4);
                                    Array.Copy(dnsserv, 0, optionData, optionData.Length - 4, 4);
                                }
                            }

                            break;
                        case DhcpOption.DomainName:
                            if (!string.IsNullOrEmpty(replyOptions.DomainName))
                            {
                                optionData = Encoding.ASCII.GetBytes(replyOptions.DomainName);
                            }

                            break;
                        case DhcpOption.ServerIdentifier:
                            if (replyOptions.ServerIdentifier != null)
                            {
                                optionData = replyOptions.ServerIdentifier.GetAddressBytes();
                            }

                            break;
                        case DhcpOption.LogServer:
                            if (replyOptions.LogServerIP != null)
                            {
                                optionData = replyOptions.LogServerIP.GetAddressBytes();
                            }

                            break;
                        case DhcpOption.StaticRoutes:
                        case DhcpOption.StaticRoutesWin:
                            if (replyOptions.StaticRoutes != null)
                            {
                                optionData = new byte[] { };
                                foreach (var route in replyOptions.StaticRoutes)
                                {
                                    var routeData = route.BuildRouteData();
                                    Array.Resize(ref optionData, optionData.Length + routeData.Length);
                                    Array.Copy(
                                        routeData,
                                        0,
                                        optionData,
                                        optionData.Length - routeData.Length,
                                        routeData.Length);
                                }
                            }

                            break;
                        default:
                            replyOptions.OtherRequestedOptions.TryGetValue(i, out optionData);
                            break;
                    }
                    if (optionData != null)
                    {
                        options[i] = optionData;
                    }
                }
            }

            if (this.GetMsgType() != DhcpMsgType.DhcpInform)
            {
                // Lease time
                if (replyOptions != null)
                {
                    options[DhcpOption.IPAddressLeaseTime] = EncodeTimeOption(replyOptions.IPAddressLeaseTime);
                    if (replyOptions.RenewalTimeValueT1.HasValue)
                    {
                        options[DhcpOption.RenewalTimeValueT1] =
                            EncodeTimeOption(replyOptions.RenewalTimeValueT1.Value);
                    }

                    if (replyOptions.RebindingTimeValueT2.HasValue)
                    {
                        options[DhcpOption.RebindingTimeValueT2] =
                            EncodeTimeOption(replyOptions.RebindingTimeValueT2.Value);
                    }
                }
            }

            // Other requested options
            if (otherForceOptions != null)
            {
                foreach (var option in otherForceOptions.Keys)
                {
                    options[option] = otherForceOptions[option];
                    if (option == DhcpOption.RelayInfo)
                    {
                        relayInfo = null;
                    }
                }
            }

            // Option 82? Send it back!
            if (relayInfo != null)
            {
                options[DhcpOption.RelayInfo] = relayInfo;
            }

            foreach (var option in options.OrderBy(x => (int)x.Key))
            {
                CreateOptionElement(ref resultOptions, option.Key, option.Value);
            }

            // Create the end option
            Array.Resize(ref resultOptions, resultOptions.Length + 1);
            Array.Copy(new byte[] { 255 }, 0, resultOptions, resultOptions.Length - 1, 1);
            return resultOptions;
        }

        /// <summary>
        /// Sends DHCP reply
        /// </summary>
        /// <param name="msgType">
        /// Type of DHCP message to send
        /// </param>
        /// <param name="ip">
        /// IP for client
        /// </param>
        /// <param name="replyData">
        /// Reply options (will be sent if requested)
        /// </param>
        /// <param name="otherForceOptions">
        /// Force reply options (will be sent anyway)
        /// </param>
        /// <param name="forceOptions">
        /// The force Options.
        /// </param>
        private void SendDhcpReply(
            DhcpMsgType msgType,
            IPAddress ip,
            DhcpReplyOptions replyData,
            Dictionary<DhcpOption, byte[]> otherForceOptions,
            IEnumerable<DhcpOption> forceOptions)
        {
            var replyBuffer = this.requestData;
            replyBuffer.Op = 2; // Reply
            replyBuffer.Yiaddr = ip.GetAddressBytes(); // Client's IP
            if (replyData.ServerIpAddress != null)
            {
                replyBuffer.Siaddr = replyData.ServerIpAddress.GetAddressBytes();
            }

            replyBuffer.Options =
                this.CreateOptionStruct(msgType, replyData, otherForceOptions, forceOptions); // Options
            if (!string.IsNullOrEmpty(this.dhcpServer.ServerName))
            {
                var serverNameBytes = Encoding.ASCII.GetBytes(this.dhcpServer.ServerName);
                var len = serverNameBytes.Length > 63 ? 63 : serverNameBytes.Length;
                Array.Copy(serverNameBytes, replyBuffer.Sname, len);
                replyBuffer.Sname[len] = 0;
            }

            lock (this.requestSocket)
            {
                var dataToSend = BuildDataStructure(replyBuffer);
                if (dataToSend.Length < 300)
                {
                    var sendArray = new byte[300];
                    Array.Copy(dataToSend, 0, sendArray, 0, dataToSend.Length);
                    dataToSend = sendArray;
                }

                if (replyBuffer.Giaddr[0] == 0 && replyBuffer.Giaddr[1] == 0 && replyBuffer.Giaddr[2] == 0
                    && replyBuffer.Giaddr[3] == 0)
                {
                    // requestSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
                    // endPoint = new IPEndPoint(dhcpServer.BroadcastAddress, PORT_TO_SEND_TO_CLIENT);
                    var udp = new UdpClient(67) { EnableBroadcast = true };
                    if (this.dhcpServer.SendDhcpAnswerNetworkInterface != null)
                    {
                        udp.Client.SetSocketOption(
                            SocketOptionLevel.IP,
                            SocketOptionName.MulticastInterface,
                            IPAddress.HostToNetworkOrder(
                                this.dhcpServer.SendDhcpAnswerNetworkInterface.GetIPProperties().GetIPv4Properties().Index));
                    }

                    udp.Send(dataToSend, dataToSend.Length, new IPEndPoint(this.dhcpServer.BroadcastAddress, 68));
                    udp.Close();
                }
                else
                {
                    this.requestSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);
                    var endPoint = new IPEndPoint(new IPAddress(replyBuffer.Giaddr), PortToSendToRelay);
                    if (this.dhcpServer.SendDhcpAnswerNetworkInterface != null)
                    {
                        this.requestSocket.SetSocketOption(
                            SocketOptionLevel.IP,
                            SocketOptionName.MulticastInterface,
                            IPAddress.HostToNetworkOrder(
                                this.dhcpServer.SendDhcpAnswerNetworkInterface.GetIPProperties().GetIPv4Properties()
                                    .Index));
                    }

                    this.requestSocket.SendTo(dataToSend, endPoint);
                }
            }
        }

        /// <summary>
        ///     Raw DHCP packet
        /// </summary>
        public struct DhcpPacket
        {
            /// <summary>Op code:   1 = boot request, 2 = boot reply</summary>
            public byte Op;

            /// <summary>Hardware address type</summary>
            public byte Htype;

            /// <summary>Hardware address length: length of MACID</summary>
            public byte Hlen;

            /// <summary>Hardware options</summary>
            public byte Hops;

            /// <summary>Transaction id</summary>
            public byte[] Xid;

            /// <summary>Elapsed time from trying to boot</summary>
            public byte[] Secs;

            /// <summary>Flags</summary>
            public byte[] Flags;

            /// <summary>Client IP</summary>
            public byte[] Ciaddr;

            /// <summary>Your client IP</summary>
            public byte[] Yiaddr;

            /// <summary>Server IP</summary>
            public byte[] Siaddr;

            /// <summary>Relay agent IP</summary>
            public byte[] Giaddr;

            /// <summary>Client HW address</summary>
            public byte[] Chaddr;

            /// <summary>Optional server host name</summary>
            public byte[] Sname;

            /// <summary>Boot file name</summary>
            public byte[] File;

            /// <summary>Magic cookie</summary>
            public byte[] Mcookie;

            /// <summary>Options (rest)</summary>
            public byte[] Options;
        }
    }
}