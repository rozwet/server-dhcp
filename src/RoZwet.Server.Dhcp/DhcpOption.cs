// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DHCPOption.cs" company="">
//   
// </copyright>
// <summary>
//   DHCP option enum
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    /// <summary>DHCP option enum</summary>
    public enum DhcpOption
    {
        /// <summary>Option 1</summary>
        SubnetMask = 1,

        /// <summary>Option 2</summary>
        TimeOffset = 2,

        /// <summary>Option 3</summary>
        Router = 3,

        /// <summary>Option 4</summary>
        TimeServer = 4,

        /// <summary>Option 5</summary>
        NameServer = 5,

        /// <summary>Option 6</summary>
        DomainNameServers = 6,

        /// <summary>Option 7</summary>
        LogServer = 7,

        /// <summary>Option 8</summary>
        CookieServer = 8,

        /// <summary>Option 9</summary>
        LprServer = 9,

        /// <summary>Option 10</summary>
        ImpressServer = 10,

        /// <summary>Option 11</summary>
        ResourceLocServer = 11,

        /// <summary>Option 12</summary>
        HostName = 12,

        /// <summary>Option 13</summary>
        BootFileSize = 13,

        /// <summary>Option 14</summary>
        MeritDump = 14,

        /// <summary>Option 15</summary>
        DomainName = 15,

        /// <summary>Option 16</summary>
        SwapServer = 16,

        /// <summary>Option 17</summary>
        RootPath = 17,

        /// <summary>Option 18</summary>
        ExtensionsPath = 18,

        /// <summary>Option 19</summary>
        IpForwarding = 19,

        /// <summary>Option 20</summary>
        NonLocalSourceRouting = 20,

        /// <summary>Option 21</summary>
        PolicyFilter = 21,

        /// <summary>Option 22</summary>
        MaximumDatagramReAssemblySize = 22,

        /// <summary>Option 23</summary>
        DefaultIPTimeToLive = 23,

        /// <summary>Option 24</summary>
        PathMtuAgingTimeout = 24,

        /// <summary>Option 25</summary>
        PathMtuPlateauTable = 25,

        /// <summary>Option 26</summary>
        InterfaceMtu = 26,

        /// <summary>Option 27</summary>
        AllSubnetsAreLocal = 27,

        /// <summary>Option 28</summary>
        BroadcastAddress = 28,

        /// <summary>Option 29</summary>
        PerformMaskDiscovery = 29,

        /// <summary>Option 30</summary>
        MaskSupplier = 30,

        /// <summary>Option 31</summary>
        PerformRouterDiscovery = 31,

        /// <summary>Option 32</summary>
        RouterSolicitationAddress = 32,

        /// <summary>Option 33</summary>
        StaticRoute = 33,

        /// <summary>Option 34</summary>
        TrailerEncapsulation = 34,

        /// <summary>Option 35</summary>
        ArpCacheTimeout = 35,

        /// <summary>Option 36</summary>
        EthernetEncapsulation = 36,

        /// <summary>Option 37</summary>
        TcpDefaultTtl = 37,

        /// <summary>Option 38</summary>
        TcpKeepaliveInterval = 38,

        /// <summary>Option 39</summary>
        TcpKeepaliveGarbage = 39,

        /// <summary>Option 40</summary>
        NetworkInformationServiceDomain = 40,

        /// <summary>Option 41</summary>
        NetworkInformationServers = 41,

        /// <summary>Option 42</summary>
        NetworkTimeProtocolServers = 42,

        /// <summary>Option 43</summary>
        VendorSpecificInformation = 43,

        /// <summary>Option 44</summary>
        NetBioSoverTcpipNameServer = 44,

        /// <summary>Option 45</summary>
        NetBioSoverTcpipDatagramDistributionServer = 45,

        /// <summary>Option 46</summary>
        NetBioSoverTcpipNodeType = 46,

        /// <summary>Option 47</summary>
        NetBioSoverTcpipScope = 47,

        /// <summary>Option 48</summary>
        XWindowSystemFontServer = 48,

        /// <summary>Option 49</summary>
        XWindowSystemDisplayManager = 49,

        /// <summary>Option 50</summary>
        RequestedIPAddress = 50,

        /// <summary>Option 51</summary>
        IPAddressLeaseTime = 51,

        /// <summary>Option 52</summary>
        OptionOverload = 52,

        /// <summary>Option 53</summary>
        DhcpMessageType = 53,

        /// <summary>Option 54</summary>
        ServerIdentifier = 54,

        /// <summary>Option 55</summary>
        ParameterRequestList = 55,

        /// <summary>Option 56</summary>
        Message = 56,

        /// <summary>Option 57</summary>
        MaximumDhcpMessageSize = 57,

        /// <summary>Option 58</summary>
        RenewalTimeValueT1 = 58,

        /// <summary>Option 59</summary>
        RebindingTimeValueT2 = 59,

        /// <summary>Option 60</summary>
        Vendorclassidentifier = 60,

        /// <summary>Option 61</summary>
        ClientIdentifier = 61,

        /// <summary>Option 62</summary>
        NetWateIPDomainName = 62,

        /// <summary>Option 63</summary>
        NetWateIPInformation = 63,

        /// <summary>Option 64</summary>
        NetworkInformationServicePlusDomain = 64,

        /// <summary>Option 65</summary>
        NetworkInformationServicePlusServers = 65,

        /// <summary>Option 66</summary>
        TftpServerName = 66,

        /// <summary>Option 67</summary>
        BootfileName = 67,

        /// <summary>Option 68</summary>
        MobileIPHomeAgent = 68,

        /// <summary>Option 69</summary>
        SmtpServer = 69,

        /// <summary>Option 70</summary>
        Pop3Server = 70,

        /// <summary>Option 71</summary>
        NntpServer = 71,

        /// <summary>Option 72</summary>
        DefaultWwwServer = 72,

        /// <summary>Option 73</summary>
        DefaultFingerServer = 73,

        /// <summary>Option 74</summary>
        DefaultIrcServer = 74,

        /// <summary>Option 75</summary>
        StreetTalkServer = 75,

        /// <summary>Option 76</summary>
        StdaServer = 76,

        /// <summary>Option 82</summary>
        RelayInfo = 82,

        /// <summary>Option 121</summary>
        StaticRoutes = 121,

        /// <summary>Option 249</summary>
        StaticRoutesWin = 249,

        /// <summary>Option 252</summary>
        Wpad = 252,

        /// <summary>Option 255 (END option)</summary>
        EndOption = 255
    }
}