﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   The byte array extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace RoZwet.Server.Dhcp
{
    using System.Text;

    /// <summary>
    /// The byte array extensions.
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// The byte array to string.
        /// </summary>
        /// <param name="ar">
        /// The ar.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string AsString(this byte[] ar)
        {
            if (ar == null)
            {
                return null;
            }

            var res = new StringBuilder();
            foreach (var b in ar)
            {
                res.Append(b.ToString("X2"));
            }

            res.Append(" (");
            foreach (var b in ar)
            {
                if (b >= 32 && b < 127)
                {
                    res.Append(Encoding.ASCII.GetString(new[] { b }));
                }
                else
                {
                    res.Append(" ");
                }
            }

            res.Append(")");
            return res.ToString();
        }
    }
}